﻿CREATE PROCEDURE [dbo].[SP_GetCentreFById]
    @id int = 0   
AS
   SELECT idCentre, nomCentre
		FROM [CentreFormation]
		WHERE [idCentre] = @id
RETURN 0
