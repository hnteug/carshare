﻿CREATE PROCEDURE [dbo].[SP_InsertDemandeC]
    @DateDemande datetime,
    @StatutDemande varchar(50),
    @IdMembre int, 
    @IdDepartStop int
AS
    INSERT INTO dbo.DemandeCovoiturage(dateDemande, statutDemande, idMembre, idDepartStop )output inserted.idDemande
VALUES (@DateDemande, @StatutDemande, @IdMembre, @IdDepartStop)
RETURN 0
