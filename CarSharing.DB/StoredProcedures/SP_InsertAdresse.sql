﻿CREATE PROCEDURE [dbo].[SP_InsertAdresse]
   @Rue VARCHAR(50),
   @Numero VARCHAR(10),
   @CodePostal VARCHAR(10),
   @Localite VARCHAR(50)
AS
   INSERT INTO dbo.Adresse(rue, numero, codePostale, localite)output inserted.idAdresse
   VALUES (@Rue, @Numero, @CodePostal, @Localite)
RETURN 0
