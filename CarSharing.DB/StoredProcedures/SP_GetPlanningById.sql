﻿CREATE PROCEDURE [dbo].[SP_GetPlanningById]
     @Id int
AS
    SELECT idPlanning, heureArrivee, dateVoyage, idOffre
    From Planning
    WHERE [idPlanning] = @Id
RETURN 0
