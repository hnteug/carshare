﻿CREATE PROCEDURE [dbo].[SP_CheckPassword]
  	@lg nvarchar(16),
	@pwd nvarchar(16)
AS
	SELECT [idMembre]
		from [Membre]
		where @lg = [login]
		and HASHBYTES('SHA2_256', [salt] + @pwd) = [Password]
