﻿CREATE PROCEDURE [dbo].[SP_InsertImage]
    @ImagePath varchar(50),
    @Titre varchar(50),
    @IdVehicule int
AS
     INSERT INTO dbo.Image( imagePath, titre, idVehicule )output inserted.idImage
VALUES (@ImagePath, @Titre, @IdVehicule)
RETURN 0
