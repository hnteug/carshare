﻿CREATE PROCEDURE [dbo].[SP_GetVehiculeById]
   @id int
AS
    SELECT idVehicule, numeroPlaque, marque,modele, idMembre 
    From Vehicule
    WHERE [idVehicule] = @id
RETURN 0
