﻿CREATE PROCEDURE [dbo].[SP_InsererMembre]
    @Nom VARCHAR(100),
	@Prenom varCHAR(20),
	@DateNaiss DATE,
	@Email Nvarchar(20), 
	@Login varchar(50),
	@Password varchar(32),
	@IDCentre INT 
--les autres parametres seront ajoutés dans la methode updateMembre car a la creation dun membre on ne connait pas 
--numeropermis etc.....	
AS
declare @salt nchar(8);
set @salt = dbo.SF_GenerateSalt();
INSERT INTO dbo.Membre(nom, prenom,dateNaiss, email, login, password,idCentre, salt ) output inserted.idMembre
VALUES (@Nom, @Prenom, @DateNaiss, @Email, @Login, HASHBYTES('SHA2_256', concat(@salt, @Password)), @IDCentre, @Salt)

RETURN 0