﻿CREATE PROCEDURE [dbo].[SP_InsertVehicule]
    @NumeroPlaque VARCHAR(10),
	@Marque VARCHAR(100),
	@Modele varCHAR(100),
	@IdMembre INT 
AS
    INSERT INTO dbo.Vehicule(numeroPlaque, marque, modele, idMembre ) output inserted.idVehicule
	VALUES (@NumeroPlaque, @Marque, @Modele, @IdMembre)
RETURN 0
