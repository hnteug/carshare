﻿CREATE PROCEDURE [dbo].[SP_UpdateMembre]
  
    @Nom VARCHAR(100),
	@Prenom varCHAR(20),
	@DateNaiss DATE,
	@Email Nvarchar(20), 
	@Login varchar(50),
	@Password varchar(32),
	@numeroPermis CHAR(10),
	@DateValiditePermis DATETIME,
	@DateValiditeCF Datetime, 
	@DateFinCF DATETIME,
	@IDCentre INT, 
	@IdMembre INT
AS
    UPDATE Membre SET nom=@Nom, 
	                  prenom=@Prenom, 
					  dateNaiss=@DateNaiss,
					  email=@Email, 
					  login=@Login, 
					  password=HASHBYTES('SHA2_256', concat((select salt from Membre where idMembre=@IdMembre), @Password)),
					  numeroPermis=@numeroPermis,
					  dateValiditePermis=@DateValiditePermis,
					  dateValiditeCF=@DateValiditeCF,
					  dateFinCF= @DateFinCF,
					   idCentre= @IDCentre   
					   WHERE  idMembre = @IdMembre 
   

