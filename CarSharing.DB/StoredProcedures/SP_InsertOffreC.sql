﻿CREATE PROCEDURE [dbo].[SP_InsertOffreC]
    @NombrePlace int,
    @DateCreation Datetime,
    @Fumeur bit,
    @Mange bit, 
    @Music bit, 
    @IdMembre INT

AS
    INSERT INTO dbo.OffreCovoiturage(nombrePlace, dateCreation, fumeur,mange, music, idMembre ) output inserted.idOffre
	VALUES (@NombrePlace, @DateCreation, @Fumeur, @Mange, @Music,  @IdMembre)
RETURN 0
