﻿CREATE PROCEDURE [dbo].[SP_InsertPlanning]
    @DateVoyage DateTime,
    @HeureArrivee DateTime, 
    @IdOffre int
AS
   INSERT INTO dbo.Planning( dateVoyage, heureArrivee, idOffre )output inserted.idPlanning
VALUES (@DateVoyage, @HeureArrivee, @IdOffre)
RETURN 0
