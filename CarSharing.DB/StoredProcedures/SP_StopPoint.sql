﻿CREATE PROCEDURE [dbo].[SP_StopPoint]
    @NomStopPint varchar(100),
    @Longitude Decimal(9,6),
    @Latitude Decimal(8,6)
AS
    INSERT INTO dbo.StopPoint( nomStopPoint, longitude, latitude )output inserted.idStop
VALUES (@NomStopPint, @Longitude, @Latitude)
RETURN 0
