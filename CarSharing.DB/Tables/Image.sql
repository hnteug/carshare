﻿CREATE TABLE [dbo].[Image]
(
   idImage INT PRIMARY KEY IDENTITY,
	imagePath VARCHAR(50) NOT NULL,
	 titre   VARCHAR (50)  NULL,
	idVehicule INT NOT NULL,
	CONSTRAINT FK_Image_Vehicule FOREIGN KEY ([idVehicule]) REFERENCES [dbo].[Vehicule] ([idVehicule])
)
