﻿CREATE TABLE [dbo].[Vehicule]
(
    [idVehicule] INT NOT NULL IDENTITY,
    numeroPlaque VARCHAR(10) NOT NULL, 
    marque VARCHAR(100) NOT NULL, 
    modele VARCHAR(100) NOT NULL, 
    [idMembre] INT NOT NULL,	
	CONSTRAINT FK_Vehicule_Membre FOREIGN KEY ([idMembre]) REFERENCES [dbo].[Membre] ([idMembre]),
    CONSTRAINT [PK_Vehicule] PRIMARY KEY ([idVehicule])
    )




