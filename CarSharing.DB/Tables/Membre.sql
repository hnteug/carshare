﻿
CREATE TABLE [dbo].[Membre]
(
    [idMembre] INT NOT NULL IDENTITY, 
    [nom] VARCHAR(100) NOT NULL,
    [prenom] VARCHAR(100) NOT NULL,
    [dateNaiss] DATE NOT NULL,
    email VARCHAR(100) not null, 
    [salt] nvarchar(8) not null,
    login varchar(50) not null,
    password varbinary(32) not null,
    numeroPermis CHAR(10) null, 
    dateValiditePermis DATETIME2 null,
    dateValiditeCF DATETIME2  null,
    dateFinCF DATETIME2  null,
    [idCentre] INT NOT NULL,
	[idAdresse] INT  NULL,
	CONSTRAINT FK_Membre_CentreF FOREIGN KEY ([idCentre]) REFERENCES [dbo].[CentreFormation] ([idCentre]),
	CONSTRAINT FK_Membre_Adresse FOREIGN KEY ([idAdresse]) REFERENCES [dbo].[Adresse] ([idAdresse]),
    CONSTRAINT [PK_Membre] PRIMARY KEY ([idMembre])
)


