﻿CREATE TABLE [dbo].[AdresseCF]
(
    [idAdresseCF] INT IDENTITY  NOT NULL,
   	[idCentre] INT NOT NULL,	
	[idAdresse] INT NOT NULL,
	CONSTRAINT FK_AdresseCF_CentreF FOREIGN KEY ([idCentre]) REFERENCES [dbo].[CentreFormation] ([idCentre]),
	CONSTRAINT FK_AdresseCF_Adresse FOREIGN KEY ([idAdresse]) REFERENCES [dbo].[Adresse] ([idAdresse]),
	CONSTRAINT  [PK_AdresseCF] PRIMARY KEY ([idAdresseCF])
)
