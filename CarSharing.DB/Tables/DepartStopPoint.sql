﻿CREATE TABLE [dbo].[DepartStopPoint]
(
    [idDepartStop] INT IDENTITY  NOT NULL,
    prixTrajet money not null,
	 heurePassage DATETIME2 not null,
	[idPlanning] INT NOT NULL,
	[idStop] INT NOT NULL,
	CONSTRAINT FK_DepartStop_StopPoint FOREIGN KEY ([idStop]) REFERENCES [dbo].[StopPoint]([idStop]),
	CONSTRAINT FK_DepartStop_Planning FOREIGN KEY ([idPlanning]) REFERENCES [dbo].[Planning]([idPlanning]), 
    CONSTRAINT [PK_DepartStopPoint] PRIMARY KEY ([idDepartStop])
)
