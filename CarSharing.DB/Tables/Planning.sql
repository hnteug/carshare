﻿CREATE TABLE [dbo].[Planning]
(
 [idPlanning] INT NOT NULL IDENTITY,
 dateVoyage DATETIME2 not null,
 heureArrivee DATETIME2 not null,
 [idOffre] INT NOT NULL,
 CONSTRAINT FK_Planning_OffreC FOREIGN KEY ([idOffre]) REFERENCES [dbo].[offreCovoiturage] ([idOffre]),
 CONSTRAINT [PK_Planning] PRIMARY KEY ([idPlanning])
)
