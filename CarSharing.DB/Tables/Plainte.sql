﻿CREATE TABLE [dbo].[Plainte]
(
    [idPlainte] INT NOT NULL IDENTITY,
    dateCreation DATETIME2 NOT NULL, 
    titre VARCHAR(100) NOT NULL,
    description  VARCHAR(100) NOT NULL,
	[idWebmaster] INT,
    [idMembre] INT NOT NULL,	
	[idMembreCreateur] INT NOT NULL,	
CONSTRAINT FK_Plainte_Webmaster FOREIGN KEY ([idWebmaster]) REFERENCES [dbo].[Webmaster] ([idWebmaster]),
CONSTRAINT FK_Plainte_Membre FOREIGN KEY ([idMembre]) REFERENCES [dbo].[Membre] ([idMembre]),
CONSTRAINT FK_Plainte_MembreCreateur FOREIGN KEY ([idMembreCreateur]) REFERENCES [dbo].[Membre] ([idMembre]),
 CONSTRAINT [PK_Plainte] PRIMARY KEY ([idPlainte])

)
