﻿CREATE TABLE [dbo].[AdminCF]
(
    [idAdmin] INT NOT NULL IDENTITY, 
    nom VARCHAR(100) not null,
    prenom VARCHAR(100) not null,
    login varchar(50) not null, 
    password varbinary(32) not null,
    idCentre INT NOT NULL,	
	CONSTRAINT  FK_AdminCF_CentreF FOREIGN KEY (idCentre) REFERENCES [dbo].[CentreFormation] ([idCentre]),
    CONSTRAINT [PK_AdminCF] PRIMARY KEY ([idAdmin])
)
