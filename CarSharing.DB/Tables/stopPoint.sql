﻿
CREATE TABLE [dbo].[StopPoint]
(
    [idStop] INT IDENTITY  NOT NULL,
    nomStopPoint VARCHAR(100) NOT NULL unique,
    longitude DECIMAL(9,6) NOT NULL,
    latitude DECIMAL(8,6) NOT NULL,
    CONSTRAINT CK_StopPoint_Longitude CHECK(longitude >= -180 AND longitude <= 180),
    CONSTRAINT CK_StopPoint_Latitude CHECK(latitude >= -90 AND latitude <= 90),
    CONSTRAINT [PK_StopPoint] PRIMARY KEY ([idStop])
)
