﻿CREATE TABLE [dbo].[DemandeCovoiturage]
(
   [idDemande] INT NOT NULL IDENTITY,
   dateDemande DATETIME2 not null,
   statutDemande VARCHAR(50) NOT NULL,
   [idDepartStop] INT NOT NULL,	
   [idMembre] INT NOT NULL,
   CONSTRAINT FK_DemandeC_DepartStop FOREIGN KEY ([idDepartStop]) REFERENCES [dbo].[DepartStopPoint] ([idDepartStop]),	
   CONSTRAINT FK_DemandeC_Membre FOREIGN KEY ([idMembre]) REFERENCES [dbo].[Membre] ([idMembre]),
   CONSTRAINT [PK_Demande] PRIMARY KEY ([idDemande])
   
)
