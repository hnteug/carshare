﻿CREATE TABLE [dbo].[Avis]
(
  [IdAvis] INT NOT NULL IDENTITY,
    [dateAvis] DATETIME2 not null,
	[note] DECIMAL(2,1) ,
    description  VARCHAR(100) NOT NULL,
   [idoffre] INT NOT NULL,		
   [idMembreCreateur] INT NOT NULL,
    CONSTRAINT CK_Avis_Note CHECK(note >= 0 AND note <= 10),
CONSTRAINT FK_Avis_MembreCreateur 	FOREIGN KEY ([idMembreCreateur]) REFERENCES [dbo].[Membre] ([idMembre]),
CONSTRAINT FK_Avis_OffreC FOREIGN KEY ([idOffre]) REFERENCES [dbo].[offreCovoiturage] ([idOffre]),
)
