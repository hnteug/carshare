﻿CREATE TABLE [dbo].[Webmaster]
(
    [idWebmaster] INT IDENTITY  NOT NULL,
    nom VARCHAR(100) not null,
    prenom VARCHAR(100) not null,
    password varbinary(32) not null,
    CONSTRAINT [PK_Webmaster] PRIMARY KEY ([idWebmaster])
    )
