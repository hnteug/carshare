﻿CREATE TABLE [dbo].[OffreCovoiturage]
(
    [idOffre] INT NOT NULL IDENTITY,
    nombrePlace SMALLINT not null,
    dateCreation DATETIME2 not null DEFAULT (GETDATE()),  
    fumeur bit NOT NULL DEFAULT (0),
    mange bit NOT NULL DEFAULT (0),
    music  bit NOT NULL DEFAULT (0),
    [idMembre] INT NOT NULL,	
	CONSTRAINT FK_OffreC_Membre FOREIGN KEY ([idMembre]) REFERENCES [dbo].[Membre] ([idMembre]),
    CONSTRAINT [PK_OffreC] PRIMARY KEY ([idOffre])
)
