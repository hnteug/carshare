﻿CREATE TABLE [dbo].[Adresse]
(
    [idAdresse] INT NOT NULL IDENTITY, 
    rue VARCHAR(50) not null, 
    numero varchar(10) not null, 
    codePostale varchar(10) not null,
    localite VARCHAR(50) not null 
    CONSTRAINT [PK_Adresse] PRIMARY KEY ([idAdresse])
)
