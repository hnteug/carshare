﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.Interfaces
{
   public  interface IOffreCovoiturageRepo
   {
        IEnumerable<OffreCovoiturage> GetAll();
        OffreCovoiturage GetOne(int Id);
        int Insert(OffreCovoiturage OffreC);
        void Update(OffreCovoiturage OffreC);
        bool Delete(int Id);
    }
}
