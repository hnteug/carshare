﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.Interfaces
{
  public  interface IStopPointRepo
    {
        IEnumerable<StopPoint> GetAll();
        StopPoint GetOne(int Id);
        int Insert(StopPoint Stop);
        void Update(StopPoint Stop);
        bool Delete(int Id);
    }
}
