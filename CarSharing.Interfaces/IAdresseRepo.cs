﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.Interfaces
{
    public interface IAdresseRepo
    {
        IEnumerable<Adresse> GetAll();
        Adresse GetOne(int Id);
        int Insert(Adresse Ad);
        void Update(Adresse Ad);
        bool Delete(int Id);
    }
}
