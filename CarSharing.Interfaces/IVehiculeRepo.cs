﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.Interfaces
{
    public interface IVehiculeRepo
    {
        int Insert(Vehicule Vh);
        void Update(Vehicule Vh);
        bool Delete(int Id);
    }
}
