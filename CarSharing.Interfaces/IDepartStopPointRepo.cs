﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.Interfaces
{
   public interface IDepartStopPointRepo
    {
        IEnumerable<DepartStopPoint> GetAll();
        DepartStopPoint GetOne(int Id);
        int Insert(DepartStopPoint DpStop);
        void Update(DepartStopPoint DpStop);
        bool Delete(int Id);
    }
}
