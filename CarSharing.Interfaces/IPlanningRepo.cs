﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.Interfaces
{
  public  interface IPlanningRepo
   {
        IEnumerable<Planning> GetAll();
        Planning GetOne(int Id);
        int Insert(Planning Pl);
        void Update(Planning PL);
        bool Delete(int Id);
    }
}
