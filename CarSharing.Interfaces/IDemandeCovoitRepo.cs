﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.Interfaces
{
    public interface IDemandeCovoitRepo
    {
        IEnumerable<DemandeCovoiturage> GetAll();
        DemandeCovoiturage GetOne(int Id);
        int Insert(DemandeCovoiturage DmdeC);
        void Update(DemandeCovoiturage DmdeC);
        bool Delete(int Id);
    }
}
