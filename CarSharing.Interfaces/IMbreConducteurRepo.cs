﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.Interfaces
{
   public interface IMbreConducteurRepo
    {
        int Insert(OffreCovoiturage offreC);

    }
}
