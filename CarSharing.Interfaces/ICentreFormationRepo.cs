﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.Interfaces
{
  public  interface ICentreFormationRepo
    {
        IEnumerable<CentreFormation> GetAll();
        CentreFormation GetOne(int Id);
        int Insert(CentreFormation CF);
        void Update(CentreFormation CF);
        bool Delete(int Id);
    }
}
