﻿using System;
using System.Collections.Generic;
using System.Text;
using CarSharing.DTO;

namespace CarSharing.Interfaces
{
    public interface IMembreRepo
    { 
        IEnumerable<Membre> GetAll();
        Membre GetOne(int Id);
        int Insert(Membre Mbre );
        void Update(Membre Mbre);
         bool Delete(int Id);
        int CheckPassword(string login, string password);
    }
}
