﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.DTO
{
   public class DemandeCovoiturage
    {
        public int IdDemande { get; set; }
        public string StatutDemande { get; set; }
        public DateTime DateDemande { get; set; }
        public int  IdMembre { get; set; }
        public int  IdDepartStop { get; set; }

    }
}
