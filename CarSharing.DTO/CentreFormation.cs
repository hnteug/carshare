﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.DTO
{
   public class CentreFormation
    {
        public int IdCentre { get; set; }
        public string NomCentre { get; set; }
    }
}
