﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.DTO
{
   public class Planning
   {
        public int IdPlanning { get; set; }
        public DateTime DateVoyage { get; set; }
        public DateTime HeureArrivee { get; set; }
        public int IdOffre { get; set; }
    }
}
