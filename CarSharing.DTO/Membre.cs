﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.DTO
{
    public class Membre
    {
        public int IdMembre { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string  NumeroPermis { get; set; }
        public DateTime DateNaiss { get; set; }
        public DateTime? DateValiditePermis { get; set; }
        public DateTime? DateValiditeCF { get; set; }
        public DateTime? DateFinCF { get; set; }
        public int IdCentre { get; set; }
        public int?  IdAdresse { get; set; }
    }
}
