﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.DTO
{
   public class DepartStopPoint
    {
        public int IdDepartStop { get; set; }
        public double PrixTrajet { get; set; }
        public DateTime HeurePassage { get; set; }
        public int IdStop { get; set; }
        public int IdPlanning { get; set; }
    }
}


