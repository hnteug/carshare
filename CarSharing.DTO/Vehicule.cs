﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.DTO
{
    public class Vehicule
    {
        public int IdVehicule { get; set; }
        public string Modele { get; set; }
        public string Marque { get; set; }
        public string NumeroPlaque { get; set; }
        public int  IdMembre { get; set; }
    }
}
