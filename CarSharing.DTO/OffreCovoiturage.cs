﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.DTO
{
    public class OffreCovoiturage
    {
        public int IdOffre { get; set; }
        public int NombrePlace { get; set; }
        public DateTime DateCreation { get; set; }
        public bool Fumeur { get; set; }
        public bool Musique { get; set; }
        public bool Mange { get; set; }
        public int IdMembre { get; set; }
    }
}
