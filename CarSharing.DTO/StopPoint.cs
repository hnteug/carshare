﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.DTO
{
   public class StopPoint
    {
        public int IdStop { get; set; }
        public string NomStopPoint { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
