﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.DTO
{
    public class AdresseCF
    {
        public int IdAdresseCF { get; set; }
        public int  IdCentre { get; set; }
        public int IdAdresse { get; set; }
    }
}
