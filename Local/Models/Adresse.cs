﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
    public class Adresse
    {
        public int IdAdresse { get; set; }
        public string Rue { get; set; }
        public string Numero { get; set; }
        public string CodePostal { get; set; }
        public string Localite { get; set; }
    }
}
