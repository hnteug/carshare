﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
   public class Plainte
    {
        public int IdPlainte { get; set; }
        public string Titre { get; set; }
        public string Description { get; set; }
        public DateTime DateCreation { get; set; }
        public Webmaster Webmaster { get; set; }
        public Membre Membre { get; set; }
    }
}
