﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
   public class Planning
    {
        public int IdPlanning { get; set; }
        public DateTime DateVoyage { get; set; }
        public DateTime HeureArrivee { get; set; }
        public OffreCovoiturage OffreCovoiturage { get; set; }
        // public DepartStopPoint DepartStopPoint { get; set; }
    }
}
