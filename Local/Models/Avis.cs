﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
    public class Avis
    {
        public int IdAvis { get; set; }
        public DateTime DateAvis { get; set; }
        public decimal Note { get; set; }
        public string Description { get; set; }
        public OffreCovoiturage OffreCovoiturage { get; set; }
        public Membre Membre { get; set; }
    }
}
