﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
   public class OffreCovoiturage
    {
        public int IdOffre { get; set; }
        public int NombrePlace { get; set; }
        public DateTime DateCreation { get; set; }
        public bool Fumeur { get; set; }
        public bool Musique { get; set; }
        public bool Mange { get; set; }
        public Membre Membre { get; set; }
        public IEnumerable<Planning> ListPlanning { get; set; }
        public IEnumerable<Avis> ListAvis { get; set; }



    }
}
