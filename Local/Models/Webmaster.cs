﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
    public class Webmaster
    {
        public int IdWebmaster { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
