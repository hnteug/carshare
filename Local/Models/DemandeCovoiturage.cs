﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
   public class DemandeCovoiturage
    {
        public int IdDemande { get; set; }
        public string StatutDemande { get; set; }
        public DateTime DateDemande { get; set; }

        public Membre Membre { get; set; }
        public DepartStopPoint DepartStopPoint { get; set; }

    }
}
