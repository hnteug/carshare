﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
  public  class Membre
    {
        public int IdMembre { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string NumeroPermis { get; set; }
        public DateTime DateNaiss { get; set; }
        public DateTime? DateValiditePermis { get; set; }
        public DateTime? DateValiditeCF { get; set; }
        public DateTime? DateFinCF { get; set; }
        public CentreFormation CentreFormation { get; set; }
        //public Adresse Adresse { get; set; }
        //public IEnumerable<Avis> ListAvis { get; set; }
        //public IEnumerable<Vehicule> ListVehicule { get; set; }
        //public IEnumerable<DemandeCovoiturage> ListDemandeC { get; set; }
        //public IEnumerable<Plainte> ListPlainte { get; set; }

     
        //public Membre(int idMbre, string nom, string prenom, string email, string login, string password, DateTime dateNaiss)
        //{
        //    IdMembre = idMbre;
        //    Nom = nom;
        //    Prenom = prenom;
        //    Email = email;
        //    Login = login;
        //    Password = password;
        //    DateNaiss = dateNaiss;
            
        //}

       


    }
}
