﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
    public class StopPoint
    {
        public int IdStop { get; set; }
        public string NomStopPoint { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
