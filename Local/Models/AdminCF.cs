﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
    public class AdminCF
    {
        public int IdAdmin { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public CentreFormation CentreFormation { get; set; }
    }
}
