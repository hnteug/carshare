﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Local.Models
{
   public class Image
    {
        public int IDImage { get; set; }
        [Required]
        [Display(Name = "Image")]
        public string ImagePath { get; set; }
        public string Titre { get; set; }
        public Vehicule Vehicule { get; set; }
    }
}
