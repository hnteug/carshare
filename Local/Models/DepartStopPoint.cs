﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
   public class DepartStopPoint
    {

        public int IdDepartStop { get; set; }
        public int PrixTrajet { get; set; }
        public DateTime HeurePassage { get; set; }
        public StopPoint StopPoint { get; set; }
        public Planning Planning { get; set; }
    }
}
