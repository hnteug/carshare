﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
   public class Vehicule
    {
        public int IdVehicule { get; set; }
        public string Modele { get; set; }
        public string Marque { get; set; }
        public string NumeroPlaque { get; set; }
        public Membre Membre { get; set; }
        //public IEnumerable<Image> ListImages { get; set; }
    }
}
