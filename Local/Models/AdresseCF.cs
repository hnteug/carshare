﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Models
{
    public class AdresseCF
    {
        public int IdAdresseCF { get; set; }
        public CentreFormation CentreFormation { get; set; }
        public Adresse Adresse { get; set; }
        
    }
}
