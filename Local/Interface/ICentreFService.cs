﻿
using Local.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Interface
{
    public interface ICentreFService
    {
        IEnumerable<CentreFormation> GetAll();
        CentreFormation GetOne(int Id);
        int Insert(CentreFormation CF);
        void Update(CentreFormation CF);
        bool Delete(int Id);
    }
}

