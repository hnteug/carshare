﻿using Local.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Interface
{
  public  interface IVehiculeService
    {
        int Insert(Vehicule Vh);
        void Update(Vehicule Vh);
        bool Delete(int Id);
    }
}
