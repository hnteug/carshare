﻿using Local.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Interface
{
   public interface IMembreService
    {
        IEnumerable<Membre> GetAll();
        Membre GetOne(int Id);
        int Insert(Membre Mbre);
        void Update(Membre Mbre);
        bool Delete(int Id);
       int CheckPassword(string login, string password);
    }
}
