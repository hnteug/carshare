﻿using Local.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Interface
{
   public interface IMbreConducteurService
    {
        int Insert(OffreCovoiturage offreC);
    }
}
