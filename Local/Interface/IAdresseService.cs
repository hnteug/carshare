﻿using Local.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Interface
{
   public interface IAdresseService
    {
        IEnumerable<Adresse> GetAll();
        Adresse GetOne(int Id);
        int Insert(Adresse Ad);
        void Update(Adresse Ad);
        bool Delete(int Id);
    }
}
