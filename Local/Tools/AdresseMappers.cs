﻿using System;
using System.Collections.Generic;
using System.Text;
using dal = CarSharing.DTO;
using local = Local.Models;
namespace Local.Tools
{
     public static class AdresseMappers
    {

        public static dal.Adresse toDal(this local.Adresse Ad)
        {
            return new dal.Adresse
            {
                IdAdresse = Ad.IdAdresse,
                Rue = Ad.Rue,
               Numero = Ad.Numero,
                CodePostal = Ad.CodePostal,
                Localite=Ad.Localite

            };
        }
    }
}
