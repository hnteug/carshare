﻿using System;
using System.Collections.Generic;
using System.Text;
using local = Local.Models;
using dal = CarSharing.DTO;

namespace Local.Tools
{
  public static class CentreFMappers
    {
        public static local.CentreFormation toLocal(this dal.CentreFormation CF)
        {
            return new local.CentreFormation
            {
                IdCentre = CF.IdCentre,
                NomCentre = CF.NomCentre,

            };
        }

        public static dal.CentreFormation toDal(this local.CentreFormation CF)
        {
            return new dal.CentreFormation 
            {
                IdCentre = CF.IdCentre,
                NomCentre = CF.NomCentre
            };
        }
    }
}
