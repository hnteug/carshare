﻿using System;
using System.Collections.Generic;
using System.Text;
using dal = CarSharing.DTO;
using local = Local.Models;

namespace Local.Tools
{
   public static class offreMapper
    {
        public static dal.OffreCovoiturage toDal(this local.OffreCovoiturage offreC)
        {
            return new dal.OffreCovoiturage
            {
               IdOffre  = offreC.IdOffre,
                IdMembre =offreC.Membre.IdMembre,
                DateCreation =offreC.DateCreation,
                Fumeur = offreC.Fumeur,
                Musique =offreC.Musique,
                Mange=offreC.Mange,
                NombrePlace=offreC.NombrePlace

            };
        }
    }
}
