﻿using Local.Models;
using System;
using System.Collections.Generic;
using System.Text;
using dal = CarSharing.DTO;
using local = Local.Models;

namespace Local.Tools
{
   public static class Mappers
    {
        public static local.Membre toLocal(this dal.Membre Mbre)
        {
            return new local.Membre
            {
                IdMembre = Mbre.IdMembre,
                Nom = Mbre.Nom,
                Prenom = Mbre.Prenom,
                Email = Mbre.Email,
                Login = Mbre.Login,
                NumeroPermis = Mbre.NumeroPermis,
                DateFinCF = Mbre.DateFinCF,
                DateValiditeCF = Mbre.DateValiditeCF,
                DateValiditePermis = Mbre.DateValiditePermis,
                Password =Mbre.Password,
                DateNaiss=Mbre.DateNaiss,
                CentreFormation=new CentreFormation()
                {IdCentre=Mbre.IdCentre }

            };
        }

        public static dal.Membre toDal(this local.Membre Mbre)
        {
            return new dal.Membre
            {
                IdMembre = Mbre.IdMembre,
                Nom = Mbre.Nom,
                Prenom = Mbre.Prenom,
                Email = Mbre.Email,
                Login = Mbre.Login,
                NumeroPermis=Mbre.NumeroPermis,
                DateFinCF=Mbre.DateFinCF,
                DateValiditeCF=Mbre.DateValiditeCF,
                DateValiditePermis=Mbre.DateValiditePermis,
                Password = Mbre.Password,
                DateNaiss = Mbre.DateNaiss,
                IdCentre=Mbre.CentreFormation.IdCentre
            };
        }

        public static dal.Vehicule toDal(this local.Vehicule Vh)
        {
            return new dal.Vehicule
            {
                IdVehicule=Vh.IdVehicule,
                IdMembre = Vh.Membre.IdMembre,
                NumeroPlaque=Vh.NumeroPlaque,
                Marque=Vh.Marque,
                Modele=Vh.Modele
            };
        }



    }
}
