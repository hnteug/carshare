﻿using CarSharing.Interfaces;
using Local.Interface;
using Local.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Local.Tools;
using System.Linq;

namespace Local.Service
{
   public class MembreService: IMembreService
    {

        private IMembreRepo _membreRepo;
        private ICentreFService _CFService;
        public MembreService(IMembreRepo MembreRepo, ICentreFService CFS)
        {
            _membreRepo = MembreRepo;
            _CFService = CFS;

        }

        public int Insert(Membre Mbre)
        {
            return _membreRepo.Insert(Mbre.toDal());
        }

        public int CheckPassword(string login, string password)
        {
            return _membreRepo.CheckPassword(login, password);
        }
        public IEnumerable<Membre> GetAll()
        {
            return _membreRepo.GetAll().Select(x => {
               Membre m= x.toLocal();
                m.CentreFormation = _CFService.GetOne(m.CentreFormation.IdCentre);
                return m;

            });
        }

        public Membre GetOne(int Id)
        {
            Membre m= _membreRepo.GetOne(Id).toLocal();
            m.CentreFormation = _CFService.GetOne(m.CentreFormation.IdCentre);
            return m;
        }


        public void Update(Membre Mbre)
        {
            _membreRepo.Update(Mbre.toDal());
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }
    }
}
