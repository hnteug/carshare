﻿using CarSharing.Interfaces;
using Local.Interface;
using Local.Models;
using Local.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Local.Service
{
    public class CentreFormationService: ICentreFService
    {

        private ICentreFormationRepo _centreFRepo;

        public CentreFormationService(ICentreFormationRepo CentreFRepo)
        {
            _centreFRepo = CentreFRepo;
        }
        public IEnumerable<CentreFormation> GetAll()
        {
            return _centreFRepo.GetAll().Select(x => x.toLocal());
        }
        public int Insert(CentreFormation CF)
        {
            return _centreFRepo.Insert(CF.toDal());
        }
        public CentreFormation GetOne(int Id)
        {
            return _centreFRepo.GetOne(Id).toLocal();
        }

        public void Update(CentreFormation CF)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }
    }
}
