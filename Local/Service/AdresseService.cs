﻿using CarSharing.Interfaces;
using Local.Interface;
using Local.Models;
using Local.Tools;
using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Service
{
   public class AdresseService: IAdresseService
    {

        private IAdresseRepo _adresseRepo;

        public AdresseService(IAdresseRepo AdresseRepo)
        {
            _adresseRepo = AdresseRepo;
        }

        public int Insert(Adresse Ad)
        {
            return _adresseRepo.Insert(Ad.toDal());
        }
        public IEnumerable<Adresse> GetAll()
        {
            throw new NotImplementedException();
        }

        public Adresse GetOne(int Id)
        {
            throw new NotImplementedException();
        }

       

        public void Update(Adresse Ad)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }
    }
}
