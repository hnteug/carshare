﻿using CarSharing.Interfaces;
using Local.Interface;
using Local.Models;
using Local.Tools;
using System;
using System.Collections.Generic;
using System.Text;

namespace Local.Service
{
   public class VehiculeService:IVehiculeService
    {
        private IVehiculeRepo _vehiculeRepo;
        public VehiculeService(IVehiculeRepo VehiculeRepo)
        {
            _vehiculeRepo = VehiculeRepo;
        }

        public int Insert(Vehicule Vh)
        {
            return _vehiculeRepo.Insert(Vh.toDal());
        }

        public void Update(Vehicule Vh)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }
    }
}
