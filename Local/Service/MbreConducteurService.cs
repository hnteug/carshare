﻿using CarSharing.Interfaces;
using Local.Interface;
using Local.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Local.Tools;
using System.Linq;

namespace Local.Service
{
   public class MbreConducteurService:IMbreConducteurService
    {
        private IMbreConducteurRepo _mbreCondRepo;

        public MbreConducteurService(IMbreConducteurRepo MbreCondRepo)
        {
            _mbreCondRepo = MbreCondRepo;
        }

        public int Insert(OffreCovoiturage offreC)
        {
            return _mbreCondRepo.Insert(offreC.toDal());
        }
    }
}
