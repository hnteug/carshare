﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarSharing.Models
{
    public class Webmaster
    {
        public int IDWebmaster { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public Webmaster()
        {
                
        }
    }
}
