﻿using CarSharing.DTO;
using CarSharing.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tools.Connection;

namespace CarSharing.Repositories
{
   public class MembreRepository: BaseRepository,IMembreRepo 
    {
      
       public MembreRepository(IConfiguration config) : base(config)
       {

       }
        public IEnumerable<Membre> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Membre");
            return _connection.ExecuteReader(cmd, Converter.MembreMapper.Convert);

        }
        public Membre GetOne(int Id)
        {
            Command cmd = new Command("SP_GetMemberById", true);
            cmd.AddParameter("Id", Id);
            return _connection.ExecuteReader(cmd, Converter.MembreMapper.Convert).FirstOrDefault();
        }

        public int Insert(Membre Mbre)
        {
            Command cmd = new Command("SP_InsererMembre", true);
            cmd.AddParameter("@Nom", Mbre.Nom);
            cmd.AddParameter("@Prenom", Mbre.Prenom);
            cmd.AddParameter("@DateNaiss ", Mbre.DateNaiss);
            cmd.AddParameter("@Email", Mbre.Email);
            cmd.AddParameter("@Login", Mbre.Login);
            cmd.AddParameter("@Password", Mbre.Password);
            cmd.AddParameter("@IDCentre", Mbre.IdCentre);
            return (int)_connection.ExecuteScalar(cmd);
        }

        public int CheckPassword(string login, string password)
        {
            Command cmd = new Command("SP_CheckPassword", true);
            cmd.AddParameter("lg", login);
            cmd.AddParameter("pwd", password);
            return (int)_connection.ExecuteScalar(cmd);
        }

        public void Update(Membre Mbre)
        {  
           Command cmd = new Command("SP_UpdateMembre", true);
            cmd.AddParameter("@Nom", Mbre.Nom);
            cmd.AddParameter("@Prenom", Mbre.Prenom);
            cmd.AddParameter("@DateNaiss ", Mbre.DateNaiss);
            cmd.AddParameter("@Email", Mbre.Email);
            cmd.AddParameter("@Login", Mbre.Login);
            cmd.AddParameter("@Password", Mbre.Password);
            cmd.AddParameter("@numeroPermis", Mbre.NumeroPermis);
            cmd.AddParameter("@DateValiditePermis", Mbre.DateValiditePermis);
            cmd.AddParameter("@DateValiditeCF", Mbre.DateValiditeCF);
            cmd.AddParameter("@DateFinCF", Mbre.DateFinCF);
            cmd.AddParameter("@IDCentre", Mbre.IdCentre);
            cmd.AddParameter("@IdMembre", Mbre.IdMembre);
             _connection.ExecuteNonQuery(cmd);

        }

        public bool Delete(int Id)
        {
            Command cmd = new Command("DELETE FROM Membre WHERE Id = @Id");
            cmd.AddParameter("Id", Id);
            return _connection.ExecuteNonQuery(cmd) == 1;
        }
    }
}
