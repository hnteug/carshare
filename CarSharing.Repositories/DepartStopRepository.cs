﻿using CarSharing.DTO;
using CarSharing.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Tools.Connection;

namespace CarSharing.Repositories
{
    public class DepartStopRepository: BaseRepository,IDepartStopPointRepo
    {
        public DepartStopRepository(IConfiguration config) : base(config)
        {

        }
        public IEnumerable<DepartStopPoint> GetAll()
        {
            Command cmd = new Command("SELECT * FROM DepartStopPoint");
            return _connection.ExecuteReader(cmd, Converter.DepartStopMapper.Convert);
        }

        public DepartStopPoint GetOne(int Id)
        {
            throw new NotImplementedException();
        }

        public int Insert(DepartStopPoint DpStop)
        {
            Command cmd = new Command("SP_InsertDepartStop", true);
            cmd.AddParameter("@PrixTrajet", DpStop.PrixTrajet);
            cmd.AddParameter("@HeurePassage", DpStop.HeurePassage);
            cmd.AddParameter("@IdPlanning", DpStop.IdPlanning);
            cmd.AddParameter("@IdStop", DpStop.IdStop);
            return (int)_connection.ExecuteScalar(cmd);
        }

        public void Update(DepartStopPoint DpStop)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }
    }
}
