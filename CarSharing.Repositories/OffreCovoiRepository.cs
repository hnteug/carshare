﻿using CarSharing.DTO;
using CarSharing.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Tools.Connection;

namespace CarSharing.Repositories
{
    public class OffreCovoiRepository:BaseRepository,IOffreCovoiturageRepo
    {
        public OffreCovoiRepository(IConfiguration config) : base(config)
        {

        }
        public IEnumerable<OffreCovoiturage> GetAll()
        {
            Command cmd = new Command("SELECT * FROM OffreCovoiturage");
            return _connection.ExecuteReader(cmd, Converter.OffreCMapper.Convert);
        }

        public int Insert(OffreCovoiturage OffreC)
        {
            Command cmd = new Command("SP_InsertOffreC", true);
            cmd.AddParameter("@NombrePlace", OffreC.NombrePlace);
            cmd.AddParameter("@DateCreation", OffreC.DateCreation);
            cmd.AddParameter("@Fumeur", OffreC.Fumeur);
            cmd.AddParameter("@Mange", OffreC.Mange);
            cmd.AddParameter("@Music", OffreC.Musique);
            cmd.AddParameter("@IdMembre", OffreC.IdMembre);
            return (int)_connection.ExecuteScalar(cmd);
        }

        public OffreCovoiturage GetOne(int Id)
        {
            throw new NotImplementedException();
        }


        public void Update(OffreCovoiturage OffreC)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }
    }
}
