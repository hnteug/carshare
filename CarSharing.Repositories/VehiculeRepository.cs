﻿using CarSharing.DTO;
using CarSharing.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Tools.Connection;

namespace CarSharing.Repositories
{
    public class VehiculeRepository: BaseRepository,IVehiculeRepo
    {
        public VehiculeRepository(IConfiguration config) : base(config)
        {

        }
       
        public int Insert(Vehicule Vh)
        {
            Command cmd = new Command("SP_InsertVehicule", true);
            cmd.AddParameter("@NumeroPlaque", Vh.NumeroPlaque);
            cmd.AddParameter("@Marque", Vh.Marque);
            cmd.AddParameter("@Modele", Vh.Modele);
            cmd.AddParameter("@IdMembre", Vh.IdMembre);
            return (int)_connection.ExecuteScalar(cmd);
        }
        public static List<Vehicule> GetVehiculeByMembre(int idMbre)
        {
            SqlCommand cmd = new SqlCommand("SELECT * from Vehicule Where idMembre= @idMembre");

            List<Vehicule> ListVh = new List<Vehicule>();
            cmd.Parameters.AddWithValue("@idMembre", idMbre);

            SqlDataReader odr = cmd.ExecuteReader();
            while (odr.Read())
            {
                Vehicule vh = new Vehicule()
                {
                    IdVehicule = (int)odr["Id"],
                    IdMembre = (int)odr["IdMembre"],
                    Modele = (string)odr["Modele"],
                    Marque = (string)odr["Marque"],
                    NumeroPlaque= (string)odr["NumeroPlaque"],
                };
                ListVh.Add(vh);
            }
            odr.Close();
           
            return ListVh;
        }
        public static List<Vehicule> GetAllVehicule()
        {
           
            SqlCommand cmd = new SqlCommand("SELECT numeroPlaque, marque,modele, idMembre  from Vehicule");

            List <Vehicule> ListVh = new List<Vehicule>();

            SqlDataReader odr = cmd.ExecuteReader();

            while (odr.Read())
            {
                
                Vehicule vh = new Vehicule()
                {
                    IdVehicule = (int)odr["Id"],
                    IdMembre = (int)odr["IdMembre"],
                    Modele = (string)odr["Modele"],
                    Marque = (string)odr["Marque"],
                    NumeroPlaque = (string)odr["NumeroPlaque"]

                };
                ListVh.Add(vh);
            }
            odr.Close();
            return ListVh;
        }
        public Vehicule GetOne(int Id)
        {
            throw new NotImplementedException();
        }
        public void Update(Vehicule Vh)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }

        //public IEnumerable<Vehicule> GetAll()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
