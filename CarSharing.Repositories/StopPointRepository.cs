﻿using CarSharing.DTO;
using CarSharing.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Tools.Connection;

namespace CarSharing.Repositories
{
   public class StopPointRepository:BaseRepository, IStopPointRepo
    {
        public  StopPointRepository(IConfiguration config) : base(config)
        {

        }
        public IEnumerable<StopPoint> GetAll()
        {
            Command cmd = new Command("SELECT * FROM StopPoint");
            return _connection.ExecuteReader(cmd, Converter.StopMapper.Convert);
        }
        public int Insert(StopPoint Stop)
        {
            Command cmd = new Command("SP_StopPoint", true);
            cmd.AddParameter("@NomStopPoint", Stop.NomStopPoint);
            cmd.AddParameter("@Longitude", Stop.Longitude);
            cmd.AddParameter("@Latitude", Stop.Latitude);
            return (int)_connection.ExecuteScalar(cmd);
        }

        public StopPoint GetOne(int Id)
        {
            throw new NotImplementedException();
        }

        

        public void Update(StopPoint Stop)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }
    }
}
