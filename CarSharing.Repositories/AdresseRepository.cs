﻿using CarSharing.DTO;
using CarSharing.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Tools.Connection;

namespace CarSharing.Repositories
{
   public class AdresseRepository: BaseRepository, IAdresseRepo
    {

        public AdresseRepository(IConfiguration config) : base(config)
        {
           
        }

        public int Insert(Adresse Ad)
        {
            Command cmd = new Command("SP_InsertAdresse", true);
            cmd.AddParameter("@Rue", Ad.Rue);
            cmd.AddParameter("@Numero", Ad.Numero);
            cmd.AddParameter("@CodePostal", Ad.CodePostal);
            cmd.AddParameter("@Localite", Ad.Localite);
            return (int)_connection.ExecuteScalar(cmd);
        }
        public IEnumerable<Adresse> GetAll()
        {
            throw new NotImplementedException();
        }

        public Adresse GetOne(int Id)
        {
            throw new NotImplementedException();
        }

        public void Update(Adresse Ad)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }

       
    }
}
