﻿using CarSharing.DTO;
using CarSharing.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Tools.Connection;

namespace CarSharing.Repositories
{
   public  class MbreConducteurRepository:BaseRepository,IMbreConducteurRepo
    {
        public MbreConducteurRepository(IConfiguration config) : base(config)
        {

        }

        public int Insert(OffreCovoiturage offreC)
        {
            Command cmd = new Command("SP_InsertOffreC", true);
            cmd.AddParameter("@NombrePlace", offreC.NombrePlace);
            cmd.AddParameter("@DateCreation", offreC.DateCreation);
            cmd.AddParameter("@Fumeur", offreC.Fumeur);
            cmd.AddParameter("@Mange", offreC.Mange);
            cmd.AddParameter("@Music", offreC.Musique);
            cmd.AddParameter("@IdMembre", offreC.IdMembre);
            return (int)_connection.ExecuteScalar(cmd);
        }
    }
}
