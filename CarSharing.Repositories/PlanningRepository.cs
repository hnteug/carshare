﻿using CarSharing.DTO;
using CarSharing.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tools.Connection;

namespace CarSharing.Repositories
{
   public class PlanningRepository: BaseRepository, IPlanningRepo
    {
        public PlanningRepository(IConfiguration config) : base(config)
        {

        }

        public int Insert(Planning Pl)
        {
            Command cmd = new Command("SP_InsertPlanning", true);
            cmd.AddParameter("@DateVoyage", Pl.DateVoyage);
            cmd.AddParameter("@HeureArrivee", Pl.HeureArrivee);
            cmd.AddParameter("@IdOffre", Pl.IdOffre);
            return (int)_connection.ExecuteScalar(cmd);
        }
        public IEnumerable<Planning> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Planning");
            return _connection.ExecuteReader(cmd, Converter.PlanningMapper.Convert);
        }

        public Planning GetOne(int Id)
        {
            Command cmd = new Command("SP_GetPlanningById", true);

            cmd.AddParameter("Id", Id);
            return _connection.ExecuteReader(cmd, Converter.PlanningMapper.Convert).FirstOrDefault();
        }


        public void Update(Planning PL)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }
    }
}
