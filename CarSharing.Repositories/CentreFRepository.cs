﻿using CarSharing.DTO;
using CarSharing.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tools.Connection;

namespace CarSharing.Repositories
{
   public class CentreFRepository: BaseRepository, ICentreFormationRepo
    {
        public CentreFRepository(IConfiguration config) : base(config)
        {

        }
        public IEnumerable<CentreFormation> GetAll()
        {
            Command cmd = new Command("SELECT * FROM CentreFormation");
            return _connection.ExecuteReader(cmd, Converter.CentreFMapper.Convert);

        }
        public CentreFormation GetOne(int Id)
        {
            Command cmd = new Command("SP_GetCentreFById", true);
            cmd.AddParameter("Id", Id);
            return _connection.ExecuteReader(cmd, Converter.CentreFMapper.Convert).FirstOrDefault();
        }

        public int Insert(CentreFormation CF)
        {
            Command cmd = new Command("SP_InsertCentreFormation", true);
            cmd.AddParameter("@NomCentre", CF.NomCentre);     
            return (int)_connection.ExecuteScalar(cmd);
        }

        public void Update(CentreFormation CF)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }
    }
}
