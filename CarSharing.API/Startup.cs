using CarSharing.Interfaces;
using Local.Interface;
using Local.Service;
using CarSharing.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.API
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //bien suivre l'odre a ce niveau add controller cors et service plus bas 
            services.AddControllers();
            services.AddCors();

            //Injecter les services en utilisant linjection de dependance
            services.AddScoped <IMembreService, MembreService>();
            services.AddScoped<IMembreRepo, MembreRepository>();

            services.AddScoped<IVehiculeService, VehiculeService>();
            services.AddScoped<IVehiculeRepo, VehiculeRepository>();

            services.AddScoped<IAdresseService, AdresseService>();
            services.AddScoped<IAdresseRepo, AdresseRepository>();
            services.AddScoped<ICentreFService, CentreFormationService>();
            services.AddScoped<ICentreFormationRepo, CentreFRepository>();

            services.AddScoped<IMbreConducteurService, MbreConducteurService>();
            services.AddScoped<IMbreConducteurRepo, MbreConducteurRepository>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CarSharing.API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {            
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CarSharing.API v1"));
            }
        
            app.UseHttpsRedirection();
            app.UseCors(c=> c.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

            });
        }
    }
}
