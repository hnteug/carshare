﻿using Microsoft.AspNetCore.Mvc;
using System;
using Local.Interface;
using Local.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CarSharing.API.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class MembreController : ControllerBase
    {

       private IMembreService _service;

        public MembreController(IMembreService mbreService)
        {
            _service = mbreService;
        }

        [HttpPost]
        [Route("api/ajoutMbre")]
        public IActionResult AjoutMbre([FromBody] Membre Mbre)
        {
            return Ok(_service.Insert(Mbre));
        }

        [HttpPost]
        [Route("api/checkpwd")]
        public int CheckPassword([FromBody] LoginForm Lgf)
        {
            return _service.CheckPassword(Lgf.Login, Lgf.Password);
        }

        [HttpGet]
        [Route("api/getAllMbre")]
        public IActionResult GetAllMbre()
        { 
            return Ok(_service.GetAll());
        }

        [HttpGet]
        [Route("api/getMbre/{Id}")]
        public IActionResult GetMbre(int Id)
        {
            return Ok(_service.GetOne(Id));
        }



        // PUT api/<MembreController>/5
        [HttpPut]
        [Route("updatedataMbre")]
        public IActionResult UpdateMbre([FromBody] Membre Mbre)
        {
            _service.Update( Mbre);
            return Ok();
        }


        //// DELETE api/<MembreController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
