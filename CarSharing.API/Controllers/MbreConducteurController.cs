﻿using Local.Interface;
using Local.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.API.Controllers
{
   // [Route("api/[controller]")]
    [ApiController]
    public class MbreConducteurController : ControllerBase
    {
        private IMbreConducteurService _service;

        public MbreConducteurController(IMbreConducteurService offreService)
        {
            _service = offreService;
        }

        [HttpPost]
        [Route("api/insererOffre")]
        public IActionResult InsererOffreC([FromBody] OffreCovoiturage offreC)
        {
            return Ok(_service.Insert(offreC));
        }
    }
}
