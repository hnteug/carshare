﻿using Local.Interface;
using Local.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CarSharing.API.Controllers
{
   // [Route("api/[controller]")]
    [ApiController]
    public class AdresseController : ControllerBase
    {

        private IAdresseService _service;

        public AdresseController(IAdresseService adService)
        {
            _service = adService;
        }



        // POST api/<AdresseController>
        [HttpPost]
        [Route("api/insererAdresse")]
        public IActionResult InsererAdresse([FromBody] Adresse Ad)
        {
            
            return Ok(_service.Insert(Ad));
        }



        //// GET: api/<AdresseController>
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<AdresseController>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        

        //// PUT api/<AdresseController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<AdresseController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
