﻿using Local.Interface;
using Local.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.API.Controllers
{
   // [Route("api/[controller]")]
    [ApiController]
    public class CentreFormationController : ControllerBase
    {
        private ICentreFService _service;

        public CentreFormationController(ICentreFService cfService)
        {
            _service = cfService;
        }

        [HttpPost]
        [Route("api/ajoutCf")]
        public IActionResult AjoutCF([FromBody] CentreFormation  CF)
        {
            return Ok(_service.Insert(CF));
        }

        [HttpGet]
        [Route("api/getCF/{Id}")]
        public IActionResult GetCentreF(int Id)
        {
            return Ok(_service.GetOne(Id));
        }

        [HttpGet]
        [Route("api/getAllCF")]
        public IActionResult GetAllCF()
        {
            return Ok(_service.GetAll());
        }
    }
}
