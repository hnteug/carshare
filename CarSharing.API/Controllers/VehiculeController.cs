﻿
using Local.Interface;
using Local.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.API.Controllers
{
   // [Route("api/[controller]")]
    [ApiController]
    public class VehiculeController : ControllerBase
    {
        private IVehiculeService _service;

        public VehiculeController(IVehiculeService vhService)
        {
            _service = vhService;
        }

        [HttpPost]
        [Route("api/insererVehicule")]
        public IActionResult InsererVehicule([FromBody] Vehicule Vh)
        {
            return Ok(_service.Insert(Vh));
        }
    }
}
