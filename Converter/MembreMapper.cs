﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Converter
{
    public static class MembreMapper
    {
        public static Membre Convert(IDataReader reader)
        {
            return new Membre
            {
                IdMembre = (int)reader["IdMembre"],
                IdCentre =(int)reader["IdCentre"],
                Nom = reader["Nom"].ToString(),
                Prenom = reader["Prenom"].ToString(),
                Email = reader["Email"].ToString(),
                Login = reader["Login"].ToString(),
                Password = reader["Password"].ToString(),
                DateNaiss = (DateTime)reader["DateNaiss"]
            };
        }

    }
}
