﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Converter
{
   public static  class OffreCMapper
    {
        public static OffreCovoiturage Convert(IDataReader reader)
        {
            return new OffreCovoiturage
            {
                IdMembre = (int)reader["IdMembre"],
                IdOffre = (int)reader["IdOffre"],
                DateCreation= (DateTime)reader["DateCreation"],
                Fumeur =(bool)reader["Fumeur"],
                Mange = (bool)reader["Mange"],
                Musique= (bool)reader["Musique"]
             
            };
        }
    }
}
