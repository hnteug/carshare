﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Converter
{
   public static class PlanningMapper
    {
        public static Planning Convert(IDataReader reader)
        {
            return new Planning
            {
                IdPlanning = (int)reader["IdPlanning"],
                IdOffre = (int)reader["IdOffre"],
                HeureArrivee= (DateTime)reader["HeureArrivee"],
                DateVoyage= (DateTime)reader["DateVoyage"]
            };
        }
    }
}
