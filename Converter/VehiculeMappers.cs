﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Converter
{
    public class VehiculeMappers
    {
        public static Vehicule Convert(IDataReader reader)
        {
            return new Vehicule
            {
                IdVehicule = (int)reader["Id"],
                IdMembre = (int)reader["IdMembre"],
                NumeroPlaque = reader["NumeroPlaque"].ToString(),
                Marque= reader["Marque"].ToString(),
                Modele=reader["Modele"].ToString(),
            };
            }
   }
}
