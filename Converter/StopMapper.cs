﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Converter
{
    public static class StopMapper
    {
        public static StopPoint Convert(IDataReader reader)
        {
            return new StopPoint
            {
                IdStop = (int)reader["IdStop"],
                NomStopPoint = (string)reader["NomStopPoint"],
                Latitude = (decimal)reader["Latitude"],
                Longitude = (decimal)reader["Longitude"]
                
            };
        }
    }
}
