﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Converter
{
   public static class CentreFMapper
    {
        public static CentreFormation Convert(IDataReader reader)
        {
            return new CentreFormation
            {
                IdCentre = (int)reader["IdCentre"],
                NomCentre = reader["NomCentre"].ToString(),
            };
        }
    }
}
