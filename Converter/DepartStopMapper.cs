﻿using CarSharing.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Converter
{
   public static class DepartStopMapper
    {
        public static DepartStopPoint Convert(IDataReader reader)
        {
            return new DepartStopPoint
            {
                IdStop = (int)reader["IdStop"],
                IdPlanning= (int)reader["IdPlanning"],
                IdDepartStop = (int)reader["IdDepartStop"],
                PrixTrajet =(double) reader["NomCentre"],
                HeurePassage=(DateTime)reader["HeurePassage"]

            };
        }
    }
}
